AUTHOR = "Albert O'Connnor"
SITENAME = "Albert O'Connor Resume"
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/Toronto'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)


DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = 'resume-theme'
CSS_FILE = 'main-6.css'


#Profile information
NAME = "Albert O'Connor"
TAGLINE = 'Software Developer'
PIC = 'albert.png'

#sidebar links
LINKEDIN = 'albertoconnor'
GITHUB = 'albertoconnor'
# TWITTER = '@iamsuheb'

CAREER_SUMMARY = 'Curious and humble software developer with over 15 years of experience from growing and mentoring startup teams to working solo freelance for many years.'

SKILLS = [
	{
		'title': 'Python',
   		'level': '90'
   	},
    {
		'title': 'Web APIs / HTTP / HTML',
   		'level': '85'
   	},
    {
		'title': 'Javascript',
   		'level': '65'
   	},
    {
		'title': 'C++ / Java',
   		'level': '25'
   	},


]

LANGUAGES = [
	{
		'name': 'English',
		'description': 'Native'
	},
]

EXPERIENCES = [
	{
		'job_title': 'Staff Software Engineer',
		'time': 'Sep 2021 - Oct 2022',
		'company': 'Bungalow',
        'points': [
            'Collaborating with senior developers, product manager, and engineering manger to design extending our existing ledger architecture to support multiple books.',
            "Supporting Bungalow's operation as true Property Manager for institution customers. Unlocking the next phase of growth of Bungalow's through the end of 2021 and in 2022.",
        ],
	},
    {
		'job_title': 'Backend Architect',
		'time': 'Oct 2020 - Aug 2021',
		'company': 'Bungalow',
        'points': [
            'Working closely with the head of product, business stakeholders, and a small engineering team to implement passing utility data from a bill aggregator applying it across multiple residents with different occupancy periods.',
            'Built and successfully managed the transition to a 1st party, ledger based platform which handled millions of dollars in transactions every month.',
        ],
	},
    {
		'job_title': 'Lead Backend Engineer',
		'time': 'Feb 2018 - Sept 2020',
		'company': 'Bungalow',
        'points': [
            'Building backend for listings website integrating with Salesforce using Django Rest Framework. Front end in Vue.js. Using Docker for development and ECS on AWS for deployment.',
            'Involved in growing the team from 3 developers to over 20.',
        ],
	},
    {
		'job_title': 'Principal',
		'time': 'May 2010 - Sept 2019',
		'company': "Albert O'Connor Web Development",
        'points': [
            'stff',
            'things'
        ],
	},



]

EDUCATIONS = [
	{
		'degree': 'BMath<br>Computer Science',
		'meta': 'University of Waterloo',
		'time': '2000 - 2005'
	},
]
